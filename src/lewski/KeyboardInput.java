package lewski;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyboardInput implements KeyListener {

	public static boolean[] keys = new boolean[120];
	private static boolean[] holdKeys = new boolean[120];
	private static boolean[] pastKeys = new boolean[120];
	
	public static boolean fwd, back, left, right, up, down;
	
	public static boolean focusToggleButton;
	public static boolean useMouseToggleButton;
	public static boolean shadowToggleButton;
	public static boolean backFaceCullingToggleButton;
		
	public static boolean key_press;
	public static char last_char;
	
	public static boolean enter;

	public static void update(){

		holdKeys[KeyEvent.VK_UP] = true;
		holdKeys[KeyEvent.VK_W] = true;
		holdKeys[KeyEvent.VK_DOWN] = true;
		holdKeys[KeyEvent.VK_S] = true;
		holdKeys[KeyEvent.VK_LEFT] = true;
		holdKeys[KeyEvent.VK_A] = true;
		holdKeys[KeyEvent.VK_RIGHT] = true;
		holdKeys[KeyEvent.VK_D] = true;
		holdKeys[KeyEvent.VK_Q] = true;
		holdKeys[KeyEvent.VK_E] = true;
				
		fwd = keys[KeyEvent.VK_UP] || keys[KeyEvent.VK_W];
		back = keys[KeyEvent.VK_DOWN] || keys[KeyEvent.VK_S];
		left = keys[KeyEvent.VK_LEFT] || keys[KeyEvent.VK_A];
		right = keys[KeyEvent.VK_RIGHT] || keys[KeyEvent.VK_D];
		
		up = keys[KeyEvent.VK_E];
		down = keys[KeyEvent.VK_Q];
		
		useMouseToggleButton = keys[KeyEvent.VK_F];
		focusToggleButton = keys[KeyEvent.VK_G];
		shadowToggleButton = keys[KeyEvent.VK_1];
		backFaceCullingToggleButton = keys[KeyEvent.VK_2];
		
		enter = keys[KeyEvent.VK_ENTER];
		
		for(int i = 0; i < keys.length; i ++){
			if(pastKeys[i] == true && holdKeys[i] != true){
				keys[i] = false;
			}
			pastKeys[i] = true;
		}
		
		key_press = false;
	}
	
	public void keyPressed(KeyEvent e) {
		key_press = true;
	
		last_char = e.getKeyChar();
		
		keys[e.getKeyCode()] = true;
		
	}

	public void keyReleased(KeyEvent e) {
		key_press = false;
		
		keys[e.getKeyCode()] = false;
	}

	public void keyTyped(KeyEvent e) {
		
	}

}
