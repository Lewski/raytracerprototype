package lewski;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import org.joml.Matrix3f;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

public class MeshObject {

	protected Vector3f position = new Vector3f(0,0,0);
	protected Vector3f rotation = new Vector3f(0,0,0);
	protected Vector3f scale;
	
	protected Vector3f origin;
	
	protected ArrayList<Vector3f> vertexPosition = new ArrayList<Vector3f>();
	protected ArrayList<Vector2f> vertexUV = new ArrayList<Vector2f>();
	protected ArrayList<Vector3f> vertexNormal = new ArrayList<Vector3f>();
	protected ArrayList<Matrix3f> faces = new ArrayList<Matrix3f>();
		
	public float luminance = 0.0f;
	public float reflection = 0.8f;
	public float transparency = 0.0f;
	public float roughness = 0.0f;
	public float indexOfRefraction = 1.45f;
			
	public Color objectColor = new Color(0,0,0);
	
	
	public boolean hasColorMap = false;
	public int[] colorMap;
	public Vector2i colorMapDimensions = new Vector2i(0,0);
	public boolean hasReflectionMap = false;
	public int[] reflectionMap;
	public Vector2i reflectionMapDimensions = new Vector2i(0,0);
	
	public MeshObject() {
		origin = calculateCenter();
	}

	public Vector3f getPosition() { return position; }
	public Vector3f getRotation() { return rotation; }
	public Vector3f getScale() { return scale; }
	public ArrayList<Vector3f> getVertexPosition() { return vertexPosition; }
	public ArrayList<Matrix3f> getFaces() { return faces; }
	
	public void setPosition(Vector3f p) {
		position = p;
	}
	
	public void setRotation(Vector3f r) {
		rotation = r;
	}
	
	public void rotate(Vector3f r) {
		Util.add(rotation, r);
		for(int i = 0; i < vertexPosition.size(); i++) {
			vertexPosition.set(i, Util.rotatePoint(vertexPosition.get(i), r));
		}
	}
	
	public void rotateAround(Vector3f axis, Vector3f r) {
		Util.add(rotation, r);
		for(int i = 0; i < vertexPosition.size(); i++) {
			vertexPosition.set(i, Util.rotateAround(vertexPosition.get(i), axis, r));
		}
	}
	
	public void setScale(Vector3f s) {
		scale = s;
	}
	public void scale(Vector3f s) {
		for(int i = 0; i < vertexPosition.size(); i++) {
			vertexPosition.set(i, Util.mult(vertexPosition.get(i), s));
		}
	}
	public void scale(float s) {
		for(int i = 0; i < vertexPosition.size(); i++) {
			vertexPosition.set(i, Util.mult(vertexPosition.get(i), s));
		}
	}
	
	public void setOrigin(Vector3f o) {
		origin = o;
	}
	
	float t = 0;
	public void update() {
		
		t += 0.01f;
		//setRotation(Util.add(getRotation(), new Vector3f(0.003f,0.003f,0)));
		//setPosition( new Vector3f((float)Math.cos(t) / 2.0f, (float)Math.sin(t) / 2.0f,(float)Math.cos(t) / 2.0f));		
	}
	
	public Vector3f calculateCenter() {
		
		float xSum = 0;
		float ySum = 0;
		float zSum = 0;
		
		for(Vector3f vert : vertexPosition)	{
			xSum += vert.x;
			ySum += vert.y;
			zSum += vert.z;
		}
		
		int c = vertexPosition.size();
		
		return new Vector3f(xSum / (float)c, ySum / (float)c, zSum / (float)c);
		
	}

	public void loadColorMap(String file) {
		
		try {
			BufferedImage image = ImageIO.read(new File(file));
			colorMapDimensions.x = image.getWidth();
			colorMapDimensions.y = image.getHeight();
			
			colorMap = new int[colorMapDimensions.x * colorMapDimensions.y];
			
			image.getRGB(0, 0, colorMapDimensions.x, colorMapDimensions.y, colorMap, 0, colorMapDimensions.x);
			
			hasColorMap = true;
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Color getColorMapValue(float u, float v) {
		int xPixel = (int) (colorMapDimensions.x * u);
		int yPixel = (int) (colorMapDimensions.y * v);
		return new Color(colorMap[xPixel + yPixel * colorMapDimensions.x]);
	}

	public void loadReflectionMap(String file) {
		
		try {
			BufferedImage image = ImageIO.read(new File(file));
			reflectionMapDimensions.x = image.getWidth();
			reflectionMapDimensions.y = image.getHeight();
			
			reflectionMap = new int[reflectionMapDimensions.x * reflectionMapDimensions.y];
			
			image.getRGB(0, 0, reflectionMapDimensions.x, reflectionMapDimensions.y, reflectionMap, 0, reflectionMapDimensions.x);
			
			hasReflectionMap = true;
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public float getReflectionMapValue(float u, float v) {
		int xPixel = (int) (reflectionMapDimensions.x * u);
		int yPixel = (int) (reflectionMapDimensions.y * v);
		Color c = new Color(reflectionMap[xPixel + yPixel * reflectionMapDimensions.x]);
		return ((c.getRed() + c.getGreen() + c.getBlue()) / 3.0f) / 255.0f;
	}
	
}
