package lewski;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import org.joml.Vector2i;

public class MouseInput implements MouseListener, MouseMotionListener, MouseWheelListener {
	
	private static boolean[] buttons = new boolean[10];
	private static boolean[] holdButtons = new boolean[10];
	private static boolean[] pastButtons = new boolean[10];
	
	public static boolean leftMouse;
	public static boolean middleMouse;
	public static boolean rightMouse;
	
	public static int deltaX;
	public static int deltaY;
	private static int prevX;
	private static int prevY;
	
	public static void update(){
		
		holdButtons[MouseEvent.BUTTON3] = true;
		
		leftMouse = buttons[MouseEvent.BUTTON1];
		middleMouse = buttons[MouseEvent.BUTTON2];
		rightMouse = buttons[MouseEvent.BUTTON3];
		
		for(int i = 0; i < buttons.length; i ++){
			if(pastButtons[i] == true && holdButtons[i] != true)
				buttons[i] = false;
			
			pastButtons[i] = true;
		}
		
		deltaX = screenMouseX - prevX;
		deltaY = screenMouseY - prevY;
		
		prevX = screenMouseX;
		prevY = screenMouseY;
		
	}
	
	public void mouseClicked(MouseEvent e) {

	}

	public void mouseEntered(MouseEvent e) {

	}

	public void mouseExited(MouseEvent e) {

	}

	public void mousePressed(MouseEvent e) {
		buttons[e.getButton()] = true;
	}

	public void mouseReleased(MouseEvent e) {
		buttons[e.getButton()] = false;
	}

	
	public static int mouseX;
	public static int mouseY;
	
	public static int screenMouseX;
	public static int screenMouseY;

	public static boolean isDragging;
	
	public void mouseDragged(MouseEvent e) {
		isDragging = true;
		mouseX = (int) ((e.getX() / RayTracer.scale) * (RayTracer.width / ((double) RayTracer.frame.getContentPane().getWidth() / (double) RayTracer.scale)));
		mouseY = (int) ((e.getY() / RayTracer.scale) * (RayTracer.width / ((double) RayTracer.frame.getContentPane().getWidth() / (double) RayTracer.scale)));
		
		screenMouseX = e.getX();
		screenMouseY = e.getY();
	}

	public void mouseMoved(MouseEvent e) {
		isDragging = false;
		mouseX = (int) ((e.getX() / RayTracer.scale) * (RayTracer.width / ((double) RayTracer.frame.getContentPane().getWidth() / (double) RayTracer.scale)));
		mouseY = (int) ((e.getY() / RayTracer.scale) * (RayTracer.width / ((double) RayTracer.frame.getContentPane().getWidth() / (double) RayTracer.scale)));
		
		screenMouseX = e.getX();
		screenMouseY = e.getY();
	}
	
	
		
	
	public static int mouseScroll = 0;

	public static int getMouseScroll(){
		int ms = mouseScroll;
		mouseScroll = 0;
		return ms;
	}
	
	public void mouseWheelMoved(MouseWheelEvent evnt) {
		mouseScroll = evnt.getWheelRotation();
		//System.out.println(evnt.getScrollType() + "\t" + evnt.getScrollAmount() + "\t" + evnt.getPreciseWheelRotation() + "\t" + evnt.getWheelRotation());
	}

}
