package lewski;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import javax.swing.JFrame;

public class RayTracer extends Canvas implements Runnable{

	public enum RenderMode { 
		RAY, //render method of casting rays from camera source and checking for light
		OPENGL //traditional render method using OpenGL, used for debugging purposes and setting up scenes in real time.
	};
	
	public RenderMode renderMode = RenderMode.RAY;
	
	public static int width = 400;
	public int height = width / 16 * 9;
	public static int scale = 4;
	
	public static Thread thread;
	private static boolean running = false;
	
	public Scene mainScene = new Scene();
	public Scene currentScene = mainScene;
	
	private BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
	private int[] pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
	
	public KeyboardInput keyInput;
	public MouseInput mouseInput;
	
	public static JFrame frame;

	int frames;
	int updates;
	
	public static int fps = 0;
	public int ups = 0;
	
	RayDisplay rayDisplay;

	public RayTracer() {
		
		Dimension size = new Dimension((int) (width * scale), (int) (height * scale));
		setPreferredSize(size);
		
		frame = new JFrame();
		frame.add(this);
		
		frame.setTitle("Ray Tracer Test");
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

		keyInput = new KeyboardInput();
		mouseInput = new MouseInput();
		
		addKeyListener(keyInput);
		addMouseListener(mouseInput);
		addMouseMotionListener(mouseInput);
		addMouseWheelListener(mouseInput);
		
		RayLightCalculator.init(mainScene);
	
		rayDisplay = new RayDisplay(currentScene, width, height);
		//OpenGLDisplay display = new OpenGLDisplay();
		
		setFocusable(true);
		frame.transferFocus();

	}
	
	public synchronized void start(){
		
		running = true;
		thread = new Thread(this, "Display");
		thread.start();
		
	}
	
	public synchronized static void stop(){
		running = false;
		System.exit(0);
		try{
			thread.join();
		} catch (InterruptedException e){
			e.printStackTrace();
		}
	}
	
	
	public void run(){
		
		long lastTime = System.nanoTime();
		long timer = System.currentTimeMillis();
		final double ns = 1_000_000_000.0 / 60.0;
		double delta = 0;
		
		frames = 0;
		updates = 0;
				
		while(running){
			
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			
			while(delta > 1){
				update();
				updates++;
				delta--;
			}
			
			
			render();
			frames++;
			
			if(System.currentTimeMillis() - timer > 1000){
				timer += 1000;
				
				fps = frames;
				ups = updates;
				
				frames = 0;
				updates = 0;
				
				frame.setTitle("FPS: " + fps + " UPS: " + ups);
			}
			
		}
		
	}
	
	public void update(){
		
		KeyboardInput.update();
		MouseInput.update();
		
		currentScene.update();
		
	}
	
	public void render(){
		
		BufferStrategy bs = getBufferStrategy();
		if(bs == null){
			createBufferStrategy(3);
			return;
		}
		
		//rayDisplay.clear();


		rayDisplay.rayTraceDisplay();
				
		for(int i = 0; i < pixels.length; i++){
			try{
				pixels[i] = rayDisplay.pixels[i];
			}catch(java.lang.ArrayIndexOutOfBoundsException e){
				System.out.println(e.getMessage());
			}
		}
		
		Graphics g = bs.getDrawGraphics();
		{
			g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
			g.setColor(new Color(20, 20, 20));
		}
		g.dispose();
		bs.show();
		
	}
	
	public static void main(String[] args) {
		RayTracer rt = new RayTracer();
		rt.start();
	}
	
}
